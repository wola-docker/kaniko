FROM gcr.io/kaniko-project/executor:debug AS kaniko

FROM alpine:3.14.2

RUN apk add --update --no-cache python3 curl tar gzip git && \
  mkdir /usr/local/gc/ && \
  curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz | tar  -C /usr/local/gc/ -xz && /usr/local/gc/google-cloud-sdk/install.sh

#
# Add kaniko to this image by re-using binaries and steps from official image
#
COPY --from=kaniko /kaniko/ /kaniko/
COPY --from=busybox:1.32.0 /bin /busybox

ENV PATH $PATH:/usr/local/bin:/kaniko:/busybox:/usr/local/gc/google-cloud-sdk/bin
ENV DOCKER_CONFIG /kaniko/.docker/
ENV DOCKER_CREDENTIAL_GCR_CONFIG /kaniko/.config/gcloud/docker_credential_gcr_config.json
